﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetzplanFormsProject
{
    class NetzplanComponent
    {
        Font drawFont = new Font("Arial", 14);
        Font smallFont = new Font("Arial", 12);
        SolidBrush drawBrush = new SolidBrush(Color.Black);
        string fazString = "1";
        string fezString = "4";

        string sazString = "1";
        string sezString = "5";

        string id = "1";
        string name = "Start";
        string dauer = "5";
        string gesamtPuffer = "5";
        string puffer = "3";

        int x = 0;
        int y = 0;

        int paddingX = Constants.paddingX;
        int paddingY = Constants.paddingY;

        public NetzplanComponent(int x, int y, string fazString, string fezString, string sazString, string sezString, string id, string name, string dauer, string gesamtPuffer, string puffer)
        {
            this.x = (x * Constants.width) + (x * (3 * Constants.paddingX));
            this.y = (y * Constants.height) + (y * (Constants.paddingY * 3));
            this.fazString = fazString;
            this.fezString = fezString;
            this.sazString = sazString;
            this.sezString = sezString;
            this.id = id;
            this.name = name;
            this.dauer = dauer;
            this.gesamtPuffer = gesamtPuffer;
            this.puffer = puffer;
        }

        public void Draw(Form form, PaintEventArgs e)
        {
            Pen blackPen = new Pen(Color.Black, 2);

            System.Drawing.Graphics formGraphics;
            formGraphics = form.CreateGraphics();
            // Komplettes Kästchen
            e.Graphics.DrawRectangle(blackPen, new Rectangle(x + paddingX, y + paddingY, Constants.width, Constants.height));
            // ID Kästschen
            e.Graphics.DrawRectangle(blackPen, new Rectangle(x + paddingX, y + paddingY, Constants.width / 3, Constants.height));
            // Unten Kästschen
            e.Graphics.DrawRectangle(blackPen, new Rectangle(x + paddingX, y + paddingY, Constants.width, Constants.height / 2));
            // Kästchen Rechtsunten
            e.Graphics.DrawRectangle(blackPen, new Rectangle(x + paddingX + Constants.width / 3 * 2, y + paddingY + Constants.height / 2, Constants.width / 3, Constants.height / 2));
            StringFormat drawFormat = new StringFormat();

            // Draw Ecken strings
            e.Graphics.DrawString(fazString, drawFont, drawBrush, x + 5, y);
            e.Graphics.DrawString(fezString, drawFont, drawBrush, x + Constants.width + 17, y);
            e.Graphics.DrawString(sazString, drawFont, drawBrush, x + 5, y + Constants.height + paddingY);
            e.Graphics.DrawString(sezString, drawFont, drawBrush, x + Constants.width + 17, y + Constants.height + paddingY);

            e.Graphics.DrawString(id, drawFont, drawBrush, x + 10 + Constants.width / 3 / 2, y + 10 + Constants.height / 4);
            e.Graphics.DrawString(name, smallFont, drawBrush, x + 20 + Constants.width / 3, y + 10 + Constants.height / 4);
            e.Graphics.DrawString(dauer, drawFont, drawBrush, x + 10 + Constants.width / 3 / 2, y + 10 + Constants.height / 2 + (Constants.height / 4));
            e.Graphics.DrawString(gesamtPuffer, drawFont, drawBrush, x + 10 + (Constants.width / 3 + Constants.width / 3 / 2), y + 10 + Constants.height / 2 + (Constants.height / 4));
            e.Graphics.DrawString(puffer, drawFont, drawBrush, x + 10 + (Constants.width / 3 * 2 + Constants.width / 3 / 2), y + 10 + Constants.height / 2 + (Constants.height / 4));

            blackPen.Dispose();
            formGraphics.Dispose();
        }

    }
}

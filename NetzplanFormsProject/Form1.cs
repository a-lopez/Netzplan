﻿using NetzplanLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetzplanFormsProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            DrawGraphic();
        }
        private void DrawGraphic()
        {

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            // Create font and brush.
            List<NetzplanLibrary.ProcessXTGui> processList = new List<ProcessXTGui>();

            NetzplanLibrary.ProcessXTGui start = new NetzplanLibrary.ProcessXTGui(0, "Start", 0);
            NetzplanLibrary.ProcessXTGui a1 = new NetzplanLibrary.ProcessXTGui(1, "A1", 1);
            NetzplanLibrary.ProcessXTGui b1 = new NetzplanLibrary.ProcessXTGui(2, "B1", 1);
            NetzplanLibrary.ProcessXTGui b2 = new NetzplanLibrary.ProcessXTGui(3, "B2", 3);
            NetzplanLibrary.ProcessXTGui end = new NetzplanLibrary.ProcessXTGui(4, "Ende", 0);

            processList.Add(start);
            processList.Add(a1);
            processList.Add(b1);
            processList.Add(b2);
            processList.Add(end);

            start.safeAddSuccessor(a1);
            a1.addPredecessor(start);
            a1.safeAddSuccessor(b1);
            a1.safeAddSuccessor(b2);
            b1.addPredecessor(a1);
            b1.safeAddSuccessor(end);
            b2.addPredecessor(a1);
            b2.safeAddSuccessor(end);
            end.addPredecessor(b1);
            end.addPredecessor(b2);

            start.calculateAllStartEndTimes();
            end.calculateAllLatestEndTimes();

            b1.calculateGP(); // => 2
            b2.calculateGP();

            NetzplanComponent netzplanComponent = new NetzplanComponent(0, 1, start.getFAZ().ToString(),
                    start.getFEZ().ToString(), start.getSAZ().ToString(), start.getSEZ().ToString(), start.getID().ToString(),
                    start.getName(), start.getDauer().ToString(), start.getFreierPuffer().ToString(), start.getPuffer().ToString());
            netzplanComponent.Draw(this, e);

            netzplanComponent = new NetzplanComponent(1, 1, a1.getFAZ().ToString(),
                    a1.getFEZ().ToString(), a1.getSAZ().ToString(), a1.getSEZ().ToString(), a1.getID().ToString(),
                    a1.getName(), a1.getDauer().ToString(), a1.getFreierPuffer().ToString(), a1.getPuffer().ToString());
            netzplanComponent.Draw(this, e);

            netzplanComponent = new NetzplanComponent(2, 0, b1.getFAZ().ToString(),
                    b1.getFEZ().ToString(), b1.getSAZ().ToString(), b1.getSEZ().ToString(), b1.getID().ToString(),
                    b1.getName(), b1.getDauer().ToString(), b1.getFreierPuffer().ToString(), b1.getPuffer().ToString());
            netzplanComponent.Draw(this, e);

            netzplanComponent = new NetzplanComponent(2, 2, b2.getFAZ().ToString(),
                    b2.getFEZ().ToString(), b2.getSAZ().ToString(), b2.getSEZ().ToString(), b2.getID().ToString(),
                    b2.getName(), b2.getDauer().ToString(), b2.getFreierPuffer().ToString(), b2.getPuffer().ToString());
            netzplanComponent.Draw(this, e);

            netzplanComponent = new NetzplanComponent(3, 1, end.getFAZ().ToString(),
                    end.getFEZ().ToString(), end.getSAZ().ToString(), end.getSEZ().ToString(), end.getID().ToString(),
                    end.getName(), end.getDauer().ToString(), end.getFreierPuffer().ToString(), end.getPuffer().ToString());
            netzplanComponent.Draw(this, e);
        }

        private void DrawProccessList(List<ProcessXTGui> processList, PaintEventArgs e)
        {
            // Funktioniert nicht!
            foreach (ProcessXTGui process in processList)
            {
                NetzplanComponent netzplanComponent = new NetzplanComponent(process.posX, process.posY, process.getFAZ().ToString(),
                    process.getFEZ().ToString(), process.getSAZ().ToString(), process.getSEZ().ToString(), process.getID().ToString(),
                    process.getName(), process.getDauer().ToString(), process.getFreierPuffer().ToString(), process.getPuffer().ToString());
                netzplanComponent.Draw(this, e);
            }
        }

        private void CalculatePositions(ProcessXTGui start, int x)
        {
            // Funktioniert nicht!
            start.posX = x;
            start.posY = 0;

            int currentX = x;
            int currentY = 0;
            foreach (ProcessXTGui process in start.getSuccessor())
            {
                process.posX = currentX;
                process.posY = currentY;
                currentY++;
            }

            foreach (ProcessXTGui process in start.getSuccessor())
            {
                CalculatePositions(process, x + 1);
            }
        }



        private void Beispiel(PaintEventArgs e)
        {
            NetzplanComponent netzplanComponent = new NetzplanComponent(0, 0, "5", "3", "1", "4", "1", "Start", "5", "5", "15");
            netzplanComponent.Draw(this, e);

            NetzplanComponent netzplanComponent2 = new NetzplanComponent(1, 0, "5", "3", "1", "4", "1", "Start", "5", "5", "15");
            netzplanComponent2.Draw(this, e);
            NetzplanComponent netzplanComponent3 = new NetzplanComponent(2, 1, "5", "3", "1", "4", "1", "Start", "5", "5", "15");
            netzplanComponent3.Draw(this, e);
        }
    }
}
